package lsg.weapons;
import lsg.weapons.Weapon;

/**
 * Classe Sword herite de Weapon
 * @author Lysandra
 *
 */

public class Sword extends Weapon{
	
	//Constructor
	/**
	 * Constructeur de la classe Sword qui initialise un nom, des d�gats min, max, un cout de stamina et une durabilite
	 */
	public Sword() {
		super("Basic Sword", 5, 10, 20, 100);
	}

}
