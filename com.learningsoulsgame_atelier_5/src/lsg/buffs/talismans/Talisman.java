package lsg.buffs.talismans;

import java.util.Calendar;

import lsg.buffs.BuffItem;

/**
 * Classe Talisman herite de BuffItem
 * @author Lysandra
 *
 */

public class Talisman extends BuffItem {
	
	/**
	 * puissance du buff
	 */
	private float buff ;
	
	/**
	 * heures � laquelle le talisman est actif ou ne l'est plus
	 */
	private int start, end ; 
	
	//Constructors
	/**
     * Constructeur de la classe Talisman
     * @param name Nom du talisman
     * @param buff Puissance du talisman
     * @param start Heure � laquelle le talisman est actif
     * @param end Heure � laquelle le talisman n'est plus actif
     */
	public Talisman(String name, float buff, int start, int end) {
		super(name) ;
		this.buff = buff ;
		this.start = start ;
		this.end = end ;
	}
	
	/**
     * Methode computeBuffValue():
     * @return retourne la valeur du buff s'il est utilis� entre ses heures de d�but et de fin d'activit� qui sont compt� en heure (0 � 23)
     * @see #getInstance()
     */
	@Override
	public float computeBuffValue() {
		int now = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) ;
		if(start <= end){
			if(now >= start && now < end) return buff ;
			else return 0f ;
		}else{
			if( (now <= 23 && now >= start) || (now >=0 && now < end)) return buff ;
			else return 0f ;
		}
	}
	
}
