package lsg;

import lsg.characters.Hero;
import lsg.characters.Lycanthrope;
import lsg.characters.Monster;
import lsg.consumables.Consumable;
import lsg.consumables.MenuBestOfV4;
import lsg.consumables.food.Hamburger;
import lsg.weapons.Sword;
import lsg.characters.Character;
import lsg.weapons.Weapon;
import lsg.weapons.ShotGun;
import lsg.weapons.*;
import lsg.armor.*;
import lsg.buffs.*;
import lsg.buffs.rings.DragonSlayerRing;
import lsg.buffs.rings.Ring;
import lsg.buffs.rings.RingOfDeath;
import lsg.buffs.rings.RingOfSwords;
import lsg.buffs.talismans.MoonStone;
import lsg.buffs.talismans.Talisman;
import lsg.consumables.*;

import java.util.Iterator;
import java.util.Scanner;

/**
 * Classe <strong>LearningSoulsGame</strong> permettant de tester l'ensemble des methodes pour lancer un combat et afficher les stats
 * @author Lysandra 
 *
 */

public class LearningSoulsGame {
	
	//Variables
	public Hero hero;
	public Monster monster;
	public Scanner scanner;
	public static final String BULLET_POINT = "\u2219";

	//Methods
	/**
	 * Methode <strong>refresh()</strong><br>
	 * Cette methode affiche les statistiques des personnages
	 */
	public void refresh() {
		this.hero.printStats();
		System.out.println(BULLET_POINT + this.hero.getWeapon().toString());
		System.out.println(BULLET_POINT + this.hero.getConsumable().toString());
		System.out.println();
		this.monster.printStats();
	}
	
	/**
	 * Methode <strong>fight1v1()</strong><br>
	 * Cette methode lance un combat entre deux personnages jusqu'� ce que l'un des deux meurt
	 */
	public void fight1v1() {
		Character firstCharacter = this.hero;
		Character opponent = this.monster;
		int action;
		refresh();
		Scanner scanner = new Scanner(System.in);
		while (firstCharacter.isAlive() && opponent.isAlive()){
			System.out.println();
			System.out.print("Hero's action for next move :  (1) attack | (2) consume > ");
			action = scanner.nextInt();
			while(action != 1 && action != 2) {
				System.out.print("Hero's action for next move :  (1) attack | (2) consume > ");
				action = scanner.nextInt();
			}
				if (action == 1) {
					int heroAttack = opponent.getHitWith(firstCharacter.attack());
					System.out.println(heroAttack);
				}
				else if (action == 2) {
					
					firstCharacter.consume();										
				}
			scanner.nextLine();			
			
			System.out.println();
			refresh();
			System.out.println();	
			int opponentAttack = firstCharacter.getHitWith(opponent.attack());
			System.out.println(opponentAttack);
			System.out.println();
			refresh();
		}
		if(firstCharacter.isAlive()) {
			System.out.println("-- " + firstCharacter.getName() + "     WINS !!!--");
		}
		else {
			System.out.println("-- " + opponent.getName() + "     WINS !!!--");
		}
		scanner.close(); 
		
	}
	
	/**
	 * Methode <strong>init()</strong><br>
	 * Cette methode permet d'initialiser les personnages, armures, armes et buffs
	 */
	public void init() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem armor = new RingedKnightArmor();
		this.hero.setArmorItem(veil, 1);
		this.hero.setArmorItem(armor, 3);
		Hamburger hamburger = new Hamburger();
		this.hero.setConsumable(hamburger);
		this.monster = new Lycanthrope();
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		title();
		System.out.println();
		
	}
	
	/**
	 * Methode <strong>play_v1()</strong><br>
	 * Cette methode initialise une partie et lance un combat
	 * @see #init()
	 * @see #fight1v1()
	 */
	public void play_v1() {
		init();
		fight1v1();
		
	}
	/**
	 * Methode <strong>play_v2()</strong><br>
	 * Cette methode initialise manuellement les personnages, armes, armures et lance un combat
	 * @see #fight1v1()
	 */
	public void play_v2() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem armor = new RingedKnightArmor();
		this.hero.setArmorItem(veil, 1);
		this.hero.setArmorItem(armor, 3);
		this.monster = new Monster("Jack the Ripper");
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		fight1v1();
		
	}
	
	/**
	 * Methode <strong>play_v3()</strong><br>
	 * Cette methode initialise manuellement les personnages, armes, armures, bufss et lance un combat
	 * @see #fight1v1()
	 */
	public void play_v3() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem armor = new DragonSlayerLeggings();
		this.hero.setArmorItem(veil, 1);
		this.hero.setArmorItem(armor, 3);
		Ring ring = new DragonSlayerRing();
		hero.setRing(ring, 1);
		this.monster = new Lycanthrope();
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		this.monster.setTalisman(new MoonStone(), 1);
		fight1v1();	
	}
	
	/**
	 * Methode <strong>createExhaustedHero()</strong><br>
	 * Methode qui cree un heros epuise
	 * 
	 */
	public void createExhaustedHero() {
		this.hero = new Hero("NewOne");
		Weapon weapon = new Weapon("Grosse Arme", 0, 0, 1000, 100);
		this.hero.setWeapon(weapon);
		this.hero.getHitWith(99);
		System.out.println();
		this.hero.attack();
		System.out.println();
		this.hero.printStats();
	}
	
	/**
	 * Methode <strong>aTable()</strong> <br>
	 * Methode qui permet � un personnage d'utiliser des consommables afin de r�cup�rer des points de vie ou d'endurance
	 */
	public void aTable() {
		Character testExHero = this.hero;
        MenuBestOfV4 menu = new MenuBestOfV4();
        Consumable consume;
        Iterator<Consumable> i = menu.iterator();
        while (i.hasNext()){
        	consume = (Consumable) i.next();
            testExHero.use(consume);
            System.out.println(testExHero.toString());
            System.out.println("Apres utilisation : " + consume.toString());
            System.out.println();            
        }
        System.out.println(testExHero.getWeapon().toString());
	}
	
	/**
	 * Methode <strong>title()</strong> qui affiche le titre du jeu
	 */
	public void title() {
		System.out.println("###############################");
		System.out.println("#   THE LEARNING SOULS GAME   #");
		System.out.println("###############################");
	}
	
	
	public static void main(String[] args) {
		//Character firstHero = new Hero();
		//Hero secondHero = new Hero("KateKane");
		//Monster monster = new Monster();
		//Monster monster1 = new Monster("JacktheRipper");
		//Monster monster2 = new Monster();
		//Weapon sword = new Sword();
		//Hero rick = new Hero("Rick");
		//Weapon shotGun = new ShotGun();
		//Weapon claw = new Claw();
		//rick.setWeapon(shotGun);
		//monster1.setWeapon(claw);
		//firstHero.printStats();
		//monster.printStats();
		//monster2.printStats();
		//secondHero.printStats();
		
		//monster1.printStats();
		//rick.getHitWith(monster1.attack());
		//rick.printStats();
		//monster1.getHitWith(rick.attack());
		//monster1.printStats();
		LearningSoulsGame firstGame = new LearningSoulsGame();
		//LearningSoulsGame secondGame = new LearningSoulsGame();
		//LearningSoulsGame thirdGame = new LearningSoulsGame();
		firstGame.play_v1();
		//secondGame.play_v2();
		//thirdGame.play_v3();
		//LearningSoulsGame testExHero = new LearningSoulsGame();
		//testExHero.createExhaustedHero();
		//testExHero.aTable();
	}

}
