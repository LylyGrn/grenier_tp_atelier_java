package lsg.weapons;
import lsg.weapons.Weapon;

/**
 * Classe ShotGun herite de Weapon
 * @author Lysandra
 *
 */

public class ShotGun extends Weapon{
	
	//Constructor
	/**
	 * Constructeur de la classe ShotGun qui initialise un nom, des d�gats min, max, un cout de stamina et une durabilite
	 */
	public ShotGun() {
		super("ShotGun", 6, 20, 5, 100);
	}

}
