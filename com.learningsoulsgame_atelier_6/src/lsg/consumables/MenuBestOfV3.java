package lsg.consumables;

import java.util.HashSet;
import java.util.Iterator;

import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Whisky;
import lsg.consumables.drinks.Wine;
import lsg.consumables.food.Americain;
import lsg.consumables.food.Hamburger;

public class MenuBestOfV3 extends HashSet<Consumable> {
	


	//Constructors

	public MenuBestOfV3() {
		Hamburger hamburger = new Hamburger();
		Wine wine = new Wine();
		Americain americain = new Americain();
		Coffee coffee = new Coffee();
		Whisky whisky = new Whisky();
		this.add(hamburger);
		this.add(wine);
		this.add(americain);
		this.add(coffee);
		this.add(whisky);

	}

	//Methods 

	public String toString() {
		int index = 0;
		System.out.print("MenuBestOfV3 : \n");
		Iterator<Consumable> i = this.iterator(); 
		while (i.hasNext()) 
			System.out.println(++index + " : " + i.next()); 
		return "";
	}


	public static void main(String[] args) {
		MenuBestOfV3 menuA = new MenuBestOfV3();
		System.out.println(menuA.toString());
	}

}
