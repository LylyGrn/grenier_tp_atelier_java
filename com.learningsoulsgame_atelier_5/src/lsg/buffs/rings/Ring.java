package lsg.buffs.rings;

import lsg.buffs.BuffItem;
import lsg.characters.Hero;

/**
 * Classe abstraite Ring herite de la classe BuffItem
 * @author Lysandra
 *
 */

public abstract class Ring extends BuffItem {
	
	/**
	 * pouvoir de la bague
	 */
	protected int power ; 
	
	/**
	 * hero de type Hero
	 */
	protected Hero hero ;
	
	/**
     * Constructeur de la classe Ring
     * @param name Nom de la bague
     * @param power Puissance de la bague
     */
	public Ring(String name, int power) {
		super(name) ;
		this.power = power ;
	}
	
	public void setHero(Hero hero) {
		this.hero = hero;
	}
	
	/**
	 * Methode getHero()
	 * @return retourne hero
	 */
	public Hero getHero() {
		return hero;
	}
	
}
