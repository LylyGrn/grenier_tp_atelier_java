package lsg.weapons;
import lsg.weapons.Weapon;

public class ShotGun extends Weapon{
	
	//Constructor
	public ShotGun() {
		super("ShotGun", 6, 20, 5, 100);
	}

}
