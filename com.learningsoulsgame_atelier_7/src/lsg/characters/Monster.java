package lsg.characters;

import java.util.ArrayList;
import java.util.List;

import lsg.armor.ArmorItem;
import lsg.buffs.BuffItem;
import lsg.buffs.talismans.Talisman;

public class Monster extends Character {
	
	//Variables
	protected static int INSTANCES_COUNT = 1;
	private float skinThickness = 20;
	protected static final int MAX_TALISMAN = 1;
	protected Talisman[] talisman;
	
	//Getters and Setters
	public float getSkinThickness() {
		return skinThickness;
	}
	public void setSkinThickness(float skinThickness) {
		this.skinThickness = skinThickness;
	}
	
	//Constructors
	
	public Monster(String name) {
		super(name);
		Monster.INSTANCES_COUNT++;
		this.setLife(10);
		this.setMaxLife(10);
		this.setStamina(10);
		this.setMaxStamina(10);
		this.setSkinThickness(20);
		this.talisman = new Talisman[MAX_TALISMAN];
		
	}
	public Monster() {
		this("Monster_" + INSTANCES_COUNT);
		

	}
	
	@Override
	protected float computeProtection() {
		return this.getSkinThickness();
		
	}
	
	public void setTalisman(Talisman talisman, int slot) {
		if (slot > 0 || slot < MAX_TALISMAN) {
			this.talisman[slot-1] = talisman;
		}
	}
	
	public float getTotalBuff() {
		float sumTalisman = 0;
		for(int index = 0; index < this.talisman.length; index++) {
			if (this.talisman[index] != null) {
				sumTalisman += this.talisman[index].computeBuffValue();				
			}
		}
		return sumTalisman;
	}
	
	public String buffToString() {
		String buffPieces = "";
		String completeBuff = "";
		for (int index = 0; index < this.talisman.length; index++) {
			if (this.talisman[index] != null) {
				buffPieces = this.talisman[index].toString();
			}
			else {
				buffPieces = "empty";
			}
			completeBuff +=  String.format(" %-1s", index+1) + String.format(":%-30s", buffPieces);
		}
		return ("BUFF    " + completeBuff + "TOTAL: " + this.getTotalBuff());
	}
		
	public BuffItem[] getBuffItems(){
		List<BuffItem> wornBuffList = new ArrayList<BuffItem>();
		for (int index = 0; index < this.talisman.length; index++) {
            if (this.talisman[index] != null) {
            	wornBuffList.add(this.talisman[index]);
            }
        }
		BuffItem[] wornBuff = wornBuffList.toArray(new BuffItem[wornBuffList.size()]);
		
		return wornBuff;
		
	}
	
	@Override
	protected float computeBuff() {
		return this.getTotalBuff();
	}

}
