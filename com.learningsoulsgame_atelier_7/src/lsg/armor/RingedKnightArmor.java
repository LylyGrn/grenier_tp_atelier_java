package lsg.armor;


/**
 * Classe RingedKnightArmor herite de la classe ArmorItem
 * @author Lysandra
 *
 */

public class RingedKnightArmor extends ArmorItem{
	
	//Constructors
	/**
	 * Constructeur de la classe RingedKnightArmor
	 * cr�e un armure (corps) avec un nom et une valeur de 14.99
	 */
	public RingedKnightArmor() {
		super("Ringed Knight Armor", (float) 14.99);
	}

}
