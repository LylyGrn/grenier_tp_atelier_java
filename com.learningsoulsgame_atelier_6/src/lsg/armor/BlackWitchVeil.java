package lsg.armor;

/**
 * Classe BlackWitchVeil herite de la classe ArmorItem
 * @author Lysandra
 *
 */

public class BlackWitchVeil extends ArmorItem{
	
	//Constructors
	/**
	 * Constructeur de la classe BlackWitchVeil
	 * cr�e un armure (corps) avec un nom et une valeur de 4.6
	 */
	public BlackWitchVeil() {
		super("Black Witch Veil", (float) 4.6);
	}

}
