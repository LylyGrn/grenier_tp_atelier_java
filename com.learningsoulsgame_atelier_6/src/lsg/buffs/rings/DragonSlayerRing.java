package lsg.buffs.rings;

import lsg.armor.ArmorItem;
import lsg.armor.DragonSlayerLeggings;

/**
 * Classe DragonSlayerRing herite de la classe Ring
 * @author Lysandra
 *
 */

public class DragonSlayerRing extends Ring{
	/**
     * Constructeur de la classe DragonSlayerRing:
     * Cr�e une bague avec un nom et une puissance de 14
     */
	public DragonSlayerRing() {
		super("Dragon Slayer Ring", 14) ;
	}
	
	
	/**
     * Methode <strong>computeBuffValue()</strong>
     * @return le valeur de la puissance si le h�ros existe et poss�de un objet de type DragonSlayerLeggings sinon retourne 0
     * @see #hasDragonsSlayerItem()
     */
	@Override
	public float computeBuffValue() {
		if(hero != null && hasDragonsSlayerItem()){
			return power ;
		}else return 0 ;
	}
	
	/**
     * Methode <strong>hasDragonsSlayerItem()</strong>
     * @return vrai si un objet DragonSlayerLeggings a �t� �quip� par un h�ros
     * @see #DragonSlayerLeggings
     * @see #getArmorItems()
     */
	private boolean hasDragonsSlayerItem(){
		ArmorItem[] items = hero.getArmorItems() ;
		for(ArmorItem item: items){
			if(item instanceof DragonSlayerLeggings) return true ; 
		}
		return false ;
	}
	
}
