package atelier_1;

import java.util.Scanner;

public class Arguments1 {

	public static void main(String[] args) {
		final String para1 = "JAVA";
		final String para2 = "PHP";
		final String para3 = "C#";
		
		int lg;		
		String message;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Quel langage utilisez-vous: JAVA = 1 ; PHP = 2 ; C# = 3 ");
		lg = scanner.nextInt();
		
		message = (lg == 1) ? para1 : para2;
		
		System.out.println(message);
		
		scanner.close();
		
		

	}

}