package lsg.consumables;

import java.util.Iterator;
import java.util.LinkedHashSet;

import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Whisky;
import lsg.consumables.drinks.Wine;
import lsg.consumables.food.Americain;
import lsg.consumables.food.Hamburger;
import lsg.consumables.repair.RepairKit;

public class MenuBestOfV4 extends LinkedHashSet<Consumable> {
	
	//Constructors

	public MenuBestOfV4() {
		Hamburger hamburger = new Hamburger();
		Wine wine = new Wine();
		Americain americain = new Americain();
		Coffee coffee = new Coffee();
		Whisky whisky = new Whisky();
		RepairKit repair = new RepairKit();
		this.add(hamburger);
		this.add(wine);
		this.add(americain);
		this.add(coffee);
		this.add(whisky);
		this.add(repair);

	}

	//Methods 

	/**
	 * Methode toString() qui permet d'afficher toutes les caracteristiques du menu et ce dont il est compos�
	 */
	public String toString() {
		int index = 0;
		System.out.print("MenuBestOfV4 : \n");
		Iterator<Consumable> i = this.iterator(); 
		while (i.hasNext()) 
			System.out.println(++index + " : " + i.next()); 
		return "";
	}


	public static void main(String[] args) {
		MenuBestOfV4 menuA = new MenuBestOfV4();
		System.out.println(menuA.toString());
	}

}
