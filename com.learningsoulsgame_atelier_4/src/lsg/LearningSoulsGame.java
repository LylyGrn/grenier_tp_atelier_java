package lsg;

import lsg.characters.Hero;
import lsg.characters.Lycanthrope;
import lsg.characters.Monster;
import lsg.weapons.Sword;
import lsg.characters.Character;
import lsg.weapons.Weapon;
import lsg.weapons.ShotGun;
import lsg.weapons.*;
import lsg.armor.*;
import lsg.buffs.*;
import lsg.buffs.rings.DragonSlayerRing;
import lsg.buffs.rings.Ring;
import lsg.buffs.rings.RingOfDeath;
import lsg.buffs.rings.RingOfSwords;
import lsg.buffs.talismans.MoonStone;
import lsg.buffs.talismans.Talisman;

import java.util.Scanner;

/**
 * Classe LearningSoulsGame permettant de tester l'ensemble des methodes pour lancer un combat et afficher les stats
 * @author Lysandra 
 *
 */

public class LearningSoulsGame {
	
	//Variables
	public Hero hero;
	public Monster monster;
	public Scanner scanner;

	//Methods
	/**
	 * Cette methode affiche les statistiques des personnages
	 */
	public void refresh() {
		this.hero.printStats();
		this.monster.printStats();
	}
	
	/**
	 * Cette methode lance un combat entre deux personnages jusqu'� ce que l'un des deux meurt
	 */
	public void fight1v1() {
		Character firstCharacter = this.hero;
		Character opponent = this.monster;
		refresh();
		Scanner scanner = new Scanner(System.in);
		while (firstCharacter.isAlive() && opponent.isAlive()){
			System.out.println();
			System.out.println("Hit enter key for next move >  ");
			scanner.nextLine();			
			int heroAttack = opponent.getHitWith(firstCharacter.attack());
			System.out.println(heroAttack);
			System.out.println();
			refresh();
			System.out.println();
			System.out.println("Hit enter key for next move >  ");
			scanner.nextLine();	
			int opponentAttack = firstCharacter.getHitWith(opponent.attack());
			System.out.println(opponentAttack);
			System.out.println();
			refresh();
		}
		if(firstCharacter.isAlive()) {
			System.out.println("-- " + firstCharacter.getName() + "     WINS !!!--");
		}
		else {
			System.out.println("-- " + opponent.getName() + "     WINS !!!--");
		}
		scanner.close(); 
		
	}
	
	/**
	 * Cette methode permet d'initialiser les personnages, armures, armes et buffs
	 */
	public void init() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem armor = new RingedKnightArmor();
		this.hero.setArmorItem(veil, 1);
		this.hero.setArmorItem(armor, 3);
		this.monster = new Monster("Jack the Ripper");
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		
	}
	
	/**
	 * Cette methode initialise une partie et lance un combat
	 * @see #init()
	 * @see #fight1v1()
	 */
	public void play_v1() {
		init();
		fight1v1();
		
	}
	/**
	 * Cette methode initialise manuellement les personnages, armes, armures et lance un combat
	 * @see #fight1v1()
	 */
	public void play_v2() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem armor = new RingedKnightArmor();
		this.hero.setArmorItem(veil, 1);
		this.hero.setArmorItem(armor, 3);
		this.monster = new Monster("Jack the Ripper");
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		fight1v1();
		
	}
	
	/**
	 * Cette methode initialise manuellement les personnages, armes, armures, bufss et lance un combat
	 * @see #fight1v1()
	 */
	public void play_v3() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem armor = new DragonSlayerLeggings();
		this.hero.setArmorItem(veil, 1);
		this.hero.setArmorItem(armor, 3);
		Ring ring = new DragonSlayerRing();
		hero.setRing(ring, 1);
		this.monster = new Lycanthrope();
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		this.monster.setTalisman(new MoonStone(), 1);
		fight1v1();	
	}
	
	
	public static void main(String[] args) {
		//Character firstHero = new Hero();
		//Hero secondHero = new Hero("KateKane");
		//Monster monster = new Monster();
		//Monster monster1 = new Monster("JacktheRipper");
		//Monster monster2 = new Monster();
		//Weapon sword = new Sword();
		//Hero rick = new Hero("Rick");
		//Weapon shotGun = new ShotGun();
		//Weapon claw = new Claw();
		//rick.setWeapon(shotGun);
		//monster1.setWeapon(claw);
		//firstHero.printStats();
		//monster.printStats();
		//monster2.printStats();
		//secondHero.printStats();
		
		//monster1.printStats();
		//rick.getHitWith(monster1.attack());
		//rick.printStats();
		//monster1.getHitWith(rick.attack());
		//monster1.printStats();
		//LearningSoulsGame firstGame = new LearningSoulsGame();
		//LearningSoulsGame secondGame = new LearningSoulsGame();
		LearningSoulsGame thirdGame = new LearningSoulsGame();
		//firstGame.play_v1();
		//secondGame.play_v2();
		thirdGame.play_v3();

	}

}
