package atelier_1;

import java.util.Scanner;

public class Bonjour0 {

	public static void main(String[] args) {
		final String FRANCAIS = "BONJOUR !";
		final String ANGLAIS = "HELLO !";
		
		int lu ;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("(1) Fran�ais / (AUTRE) Anglais > ");
		lu = scanner.nextInt();
		
		if(lu == 1) {
			System.out.println(FRANCAIS);
		}
		else {
			System.out.println(ANGLAIS);

			
		}
		scanner.close();

	}

}

// Ce programme invite l'utilisateur � entrer un entier (1 ou autre chose), et renvoit soit BONJOUR !, soit HELLO ! selon le chiffre 
//entr�. 