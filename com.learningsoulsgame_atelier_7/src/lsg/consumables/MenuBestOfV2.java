package lsg.consumables;

import java.util.HashSet;
import java.util.Iterator;

import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Whisky;
import lsg.consumables.drinks.Wine;
import lsg.consumables.food.Americain;
import lsg.consumables.food.Hamburger;

public class MenuBestOfV2 {
	
	
	//Variables
	
	//protected Consumable[] menu = new Consumable[5];
	public HashSet<Consumable> menu;
		
		
	//Constructors
		
	public MenuBestOfV2() {
		Hamburger hamburger = new Hamburger();
		Wine wine = new Wine();
		Americain americain = new Americain();
		Coffee coffee = new Coffee();
		Whisky whisky = new Whisky();
		this.menu = new HashSet<Consumable>();
		menu.add(hamburger);
		menu.add(wine);
		menu.add(americain);
		menu.add(coffee);
		menu.add(whisky);

	}
		
	//Methods 

	public String toString() {
		int index = 0;
		System.out.print("MenuBestOfV2 : \n");
		Iterator<Consumable> i = menu.iterator(); 
        while (i.hasNext()) 
            System.out.println(++index + " : " + i.next()); 
		return "";
	}


	public static void main(String[] args) {
		MenuBestOfV2 menuA = new MenuBestOfV2();
		System.out.println(menuA.toString());
	}

}
