package lsg.armor;

/**
 * Classe DragonSlayerLeggings herite de la classe ArmorItem
 * @author Lysandra
 *
 */

public class DragonSlayerLeggings extends ArmorItem{
	
	//Constructors
	/**
	 * Constructeur de la classe DragonSlayerLeggings
	 * cr�e un armure (jambes) avec un nom et une valeur de 10.2
	 */
	public DragonSlayerLeggings() {
		super("Dragon Slayer Leggings", (float) 10.2);
	}

}
