package lsg.helpers;

import java.util.Random;

/**
 * Classe Dice
 * @author Lysandra
 *
 */

public class Dice {
	
	//Variables
	/**
	 * nombre de faces du d�
	 */
	public int faces;
	
	/**
	 * nombre aleatoire
	 */
	public Random random;
	
	//Constructor
	/**
	 * Constructeur de la classe Dice
	 * @param faces nombre de faces du d�
	 */
	public Dice(int faces) {
		this.faces = faces;
		random = new Random();
		
	}
	
	//Method
	/**
	 * Methode <strong>roll()</strong>
	 * @return retourne un nombre aleatoire correspondant � un lanc� de d�
	 */
	public int roll(){
		return random.nextInt(faces);
	}
	
	//Dice roll test
	public static void main(String[] args) {
		Dice dice = new Dice(50);
		int max = 0;
		int min = 0;
		for(int i = 0; i < 500; i++ ) {
			int result = dice.roll();
			System.out.print(result + " ");
			if(result>max) {
				max = result;
			}
			if(result<min) {
				min = result;
			}
			
		}
		System.out.println("\nMin : " + min);
		System.out.println("Max : " + max);
		
		
		
	}
	

}
