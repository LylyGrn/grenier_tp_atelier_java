package lsg;

import characters.Hero;
import characters.Monster;

public class LearningSoulsGame {

	
	public static void main(String[] args) {
		Hero firstHero = new Hero();
		Hero secondHero = new Hero("KateKane");
		Monster monster = new Monster();
		Monster monster1 = new Monster("JacktheRipper");
		Monster monster2 = new Monster();
		firstHero.printStats();
		secondHero.printStats();
		monster.printStats();
		monster1.printStats();
		monster2.printStats();
		firstHero.isAlive();

	}

}
