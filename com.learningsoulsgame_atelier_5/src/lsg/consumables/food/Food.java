package lsg.consumables.food;

import lsg.consumables.Consumable;

public class Food extends Consumable {

	public Food(String name, int capacity, String stat) {
		super(name, capacity, stat);
	}

}
