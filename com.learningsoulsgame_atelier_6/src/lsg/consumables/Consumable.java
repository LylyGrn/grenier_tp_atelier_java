package lsg.consumables;

import lsg.bags.Collectible;

public class Consumable implements Collectible{
	
	//Variables
	
	private String name;
	private int capacity;
	private String stat;
	
	
	//Constructors 
	
	public Consumable(String name, int capacity, String stat) {
		this.name = name;
		this.capacity = capacity;
		this.stat = stat;
	}
	
	//Getters and Setters
	
	/**
	 * @return Retourne le nom du consommable
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return Retourne la capacit� du consommable
	 */
	public int getCapacity() {
		return capacity;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	/**
	 * @return Retourne le nom de la statistique � laquelle le consommable est d�di�
	 */
	public String getStat() {
		return stat;
	}
	
	//Methods
	
	/**
	 * Methode <strong>toString()</strong>:<br>
	 * Methode qui permet d'afficher les statistiques des consommables
	 * @returns Retourne les statistiques d'un consommables
	 */
	public String toString() {
		return (String.format("%-2s", this.name) + " [" + this.capacity + " " + this.stat + " point(s)]");
	}
	
	/**
	 * Methode <strong>use()</strong>:<br>
	 * Methode qui permet d'utiliser un consommable et met sa capacite a zero apres utilisation
	 * @return Retourne la capacite
	 */
	public int use() {
		int capacity = this.getCapacity();
		this.setCapacity(0);
		return capacity;
	}

	/**
	 * Methode <strong>getWeight()</strong>
	 * @return un poids de 1 kg
	 */
	@Override
	public int getWeight() {
		return 1;
	}

}
