package lsg.weapons;

import lsg.bags.Collectible;
import lsg.consumables.repair.RepairKit;

/**
 * Classe Weapon
 * @author Lysandra
 *
 */

public class Weapon implements Collectible {
	
	//Variables
	/**
	 * nom de l'arme
	 */
	private String name;
	
	/**
	 * degats minimum
	 */
	private int minDamage;
	
	/**
	 * degats maximum
	 */
	private int maxDamage;
	
	/**
	 * cout de stamina
	 */
	private int stamCost;
	
	/**
	 * durabilite
	 */
	private int durability;
	
	
	public final static String DURABILITY_STAT_STRING = "durability";
	
	//Constructor
	/**
	 * Constructeur de la classe Weapon
	 * @param name nom de l'arme
	 * @param minDamage degats minimum
	 * @param maxDamage degats maximum
	 * @param stamCost cout stamina
	 * @param durability durabilite
	 */
	public Weapon(String name, int minDamage, int maxDamage, int stamCost, int durability) {
		this.name= name;
		this.minDamage = minDamage;
		this.maxDamage = maxDamage;
		this.stamCost = stamCost;
		this.durability = durability;		
	}
	
	public Weapon() {
		
	}
	
	//Getters and Setters
	public String getName() {
		return name;
	}
	
	public int getMinDamage() {
		return minDamage;
	}
	
	public int getMaxDamage() {
		return maxDamage;
	}
	
	public int getStamCost() {
		return stamCost;
	}
	
	public int getDurability() {
		return durability;
	}
	private void setDurability(int durability) {
		this.durability = durability;
	}
	
	//Methods
	/**
	 * Methode <strong>use()</strong>:<br>
	 * Methode qui permet d'enlever un point � la durabilit� de l'arme � chaque utilisation de celle-ci
	 * @return Retourne la durabilite apr�s utilisation de l'arme
	 */
	public int use() {
		return this.durability -= 1;
	}
	
	/**
	 * Methode <strong>isBroken()</strong>:<br>
	 * Methode qui permet de savoir si une arme est cassee ou non
	 * @return Retourne vrai si la durabilite de l'arme est inferieure ou egale a 0
	 */
	public boolean isBroken() {
		return this.durability <=0;
	}
	
	/**
	 * Methode <strong>toString()</strong> pour les armes
	 * @returns Retourne les statistiques d'une arme 
	 */
	public String toString() {
		return (String.format("%-2s", name) + " (min:" + minDamage + " max:" + maxDamage + " stam:" + stamCost + " " + Weapon.DURABILITY_STAT_STRING +" :" + durability + ") ");
		
	}

	public void printStats() {
		System.out.println(this.toString());
	}
	
	/**
	 * Methode <strong>repairWith(RepairKit kit)</strong>:<br>
	 * Methode qui remonte la durabilite de l'arme d'un point par utilisation 
	 * @param kit kit de reparation
	 */
	public void repairWith(RepairKit kit) {
		setDurability(getDurability() + kit.use());
		
	}

	/**
	 * Methode <strong>getWeight()</strong>
	 * @return un poids de 2 kg
	 */
	@Override
	public int getWeight() {
		return 2;
	}

}
