package lsg.helpers;

import java.util.Random;

public class Dice {
	
	//Variables
	public int faces;
	public Random random;
	
	//Constructor
	public Dice(int faces) {
		this.faces = faces;
		random = new Random();
		
	}
	
	//Method
	public int roll(){
		return random.nextInt(faces);
	}
	
	//Dice roll test
	public static void main(String[] args) {
		Dice dice = new Dice(50);
		int max = 0;
		int min = 0;
		for(int i = 0; i < 500; i++ ) {
			int result = dice.roll();
			System.out.print(result + " ");
			if(result>max) {
				max = result;
			}
			if(result<min) {
				min = result;
			}
			
		}
		System.out.println("\nMin : " + min);
		System.out.println("Max : " + max);
		
		
		
	}
	

}
