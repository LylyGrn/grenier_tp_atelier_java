package lsg.characters;
import java.util.ArrayList;
import java.util.List;

import lsg.buffs.BuffItem;
import lsg.buffs.rings.DragonSlayerRing;
import lsg.buffs.rings.Ring;
import lsg.armor.*;

public class Hero extends Character {
	
	//Variables
	protected ArmorItem[] armor;
	protected static final int MAX_ARMOR_PIECES = 3;
	protected Ring[] ring;
	protected static final int MAX_RINGS = 2;
	
	//Constructors
	
	public Hero(String name) {
		super(name);
		this.setLife(100);
		this.setMaxLife(100);
		this.setStamina(50);
		this.setMaxStamina(50);
		this.armor = new ArmorItem[MAX_ARMOR_PIECES];
		this.ring = new Ring[MAX_RINGS];
		
	}
	public Hero() {
		this.name = "Gregooninator";
		this.setLife(100);
		this.setMaxLife(100);
		this.setStamina(50);
		this.setMaxStamina(50);
		
	}
	
	//Getters and Setters
	public Ring[] getRing() {
		return ring;
		
	}
	public void setRing(Ring ring, int slot) {
		if (slot > 0 && slot <= MAX_RINGS) {
			this.ring[slot-1] = ring;
			ring.setHero(this);
		}
	}
	
	public Ring[] getRings(){
        int tabRingSize = MAX_RINGS;
        for (Ring ring : this.ring) {
            if (ring == null) {
                tabRingSize--;
            }
        }

        int i = 0;
        Ring[] tabRing = new Ring[tabRingSize];
        for (Ring ring : this.ring) {
            if (ring != null) {
                tabRing[i] = ring;
                i++;
            }
        }

        return tabRing;
    }
	
	
	//Methods 
	public void setArmorItem(ArmorItem armorPiece, int slot) {
		if (slot > 0 || slot < MAX_ARMOR_PIECES) {
			this.armor[slot-1] = armorPiece;
		}
	}

	
	public float getTotalArmor() {
		float sumArmors = 0;
		for(int index = 0; index < this.armor.length; index++) {
			if (this.armor[index] != null) {
				sumArmors += this.armor[index].getArmorValue();				
			}
		}
		return sumArmors;
	}
	
	public float getTotalBuff() {
		float sumRing = 0;
		for(int index = 0; index < this.ring.length; index++) {
			if (this.ring[index] != null) {
				sumRing += this.ring[index].computeBuffValue();				
			}
		}
		return sumRing;
	}
	
	public String armorToString() {
		String armorPieces = "";
		String completeArmor = "";
		for (int index = 0; index < this.armor.length; index++) {
			if (this.armor[index] != null) {
				armorPieces = this.armor[index].toString();
			}
			else {
				armorPieces = "empty";
			}
			completeArmor +=  String.format(" %-1s", index+1) + String.format(":%-30s", armorPieces);
		}
		return ("ARMOR    " +completeArmor + "TOTAL: " + this.getTotalArmor());
	}
	
	public String buffToString() {
		String buffPieces = "";
		String completeBuff = "";
		for (int index = 0; index < this.ring.length; index++) {
			if (this.ring[index] != null) {
				buffPieces = this.ring[index].toString();
			}
			else {
				buffPieces = "empty";
			}
			completeBuff +=  String.format(" %-1s", index+1) + String.format(":%-30s", buffPieces);
		}
		return (completeBuff + "TOTAL: " + this.getTotalBuff());
	}
	
	public ArmorItem[] getArmorItems(){
		List<ArmorItem> wornArmorList = new ArrayList<ArmorItem>();
		for (int index = 0; index < this.armor.length; index++) {
            if (this.armor[index] != null) {
            	wornArmorList.add(this.armor[index]);
            }
        }
		ArmorItem[] wornArmor = wornArmorList.toArray(new ArmorItem[wornArmorList.size()]);
		
		return wornArmor;
		
	}
	
	
	@Override
	protected float computeProtection() {
		return this.getTotalArmor();
	}
	
	@Override
	protected float computeBuff() {
		return this.getTotalBuff();
	}
	
	public BuffItem[] getBuffItems(){
		List<BuffItem> wornBuffList = new ArrayList<BuffItem>();
		for (int index = 0; index < this.ring.length; index++) {
            if (this.ring[index] != null) {
            	wornBuffList.add(this.ring[index]);
            }
        }
		BuffItem[] wornBuff = wornBuffList.toArray(new BuffItem[wornBuffList.size()]);
		
		return wornBuff;
		
	}
	
	public void equip(ArmorItem item, int slot) {
		if (pullOut(item) != null) {
			this.setArmorItem(item, slot);
			System.out.print(" and equips it ! ");
		}
	}
	
	public void equip(Ring ring, int slot) {
		if (pullOut(ring) != null) {
			this.setRing(ring, slot);
			System.out.print(" and equips it ! ");
		}
	}
	
	public void printRings() {
		if(ring != null) {
			System.out.println("RINGS    " + buffToString());
		}
		else {
			System.out.println("RINGS   :empty ");
		}
	}

	
	public static void main(String[] args) {
		Hero hero = new Hero("Kate Kane");
		//ArmorItem leggings = new DragonSlayerLeggings();
		ArmorItem armor = new RingedKnightArmor();
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem leggings = new DragonSlayerLeggings();
		Ring ring = new DragonSlayerRing();
		hero.setArmorItem(leggings, 1);
		hero.setArmorItem(veil, 2);
		hero.setArmorItem(armor, 3);
		hero.setRing(ring, 1);
		System.out.println(hero.armorToString());
		System.out.println(hero.getArmorItems());
		System.out.println(hero.buffToString());
	}
	
	
}
