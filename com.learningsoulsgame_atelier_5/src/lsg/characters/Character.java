package lsg.characters;
import lsg.armor.ArmorItem;
import lsg.buffs.BuffItem;
import lsg.consumables.Consumable;
import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Drink;
import lsg.consumables.food.Food;
import lsg.consumables.food.Hamburger;
import lsg.consumables.repair.RepairKit;
import lsg.helpers.Dice;
import lsg.weapons.Weapon;
import java.lang.Math;
import java.util.Locale; 

public abstract class Character {
	
	//Variables
	protected String name;
	protected int life;
	protected int maxLife;
	protected int stamina;
	protected int maxStamina;
	protected Dice dice;
	protected Weapon weapon;
	protected Consumable consumable;
	
	
	public final static String LIFE_STAT_STRING = "LIFE";
	public final static String STAM_STAT_STRING = "STAMINA";
	
	//Getters and Setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getLife() {
		return life;
	}
	public void setLife(int life) {
		this.life = life;
	}
	
	public int getMaxLife() {
		return maxLife;
	}
	public void setMaxLife(int maxLife) {
		this.maxLife = maxLife;
	}
	
	public int getStamina() {
		return stamina;
	}
	public void setStamina(int stamina) {
		this.stamina = stamina;
	}
	
	public int getMaxStamina() {
		return maxStamina;
	}
	public void setMaxStamina(int maxStamina) {
		this.maxStamina = maxStamina;
	}
	
	public Weapon getWeapon() {
		return weapon;
	}
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}
	
	public Consumable getConsumable() {
		return consumable;
	}
	public void setConsumable(Consumable consumable) {
		this.consumable = consumable;
	}
	
	//Constructor
	public Character(String name) {
		this.name = name;
		this.dice = new Dice(101);
		this.weapon = new Weapon("Basic Sword", 5, 10, 20, 100);
	}
	
	public Character() {
		
	}
	
	//Methods

	public String toString() {
		if (isAlive() == true) {
			return (String.format("[ %-7s ] ", getClass().getSimpleName()) + String.format("   %-20s", name) + String.format(Character.LIFE_STAT_STRING + " : %-15s  ", life) + String.format(Character.STAM_STAT_STRING + " :  %-15s  ", stamina)  + String.format(Locale.US, ArmorItem.ARMOR_STAT_STRING + " :  %-15s  ", this.computeProtection()) + String.format(Locale.US, BuffItem.BUFF_STAT_STRING + " :  %-15s  ", this.computeBuff()) +"(ALIVE)");
		}
		else {
			return (String.format("[ %-7s ] ", getClass().getSimpleName()) + String.format("   %-20s", name) + String.format(Character.LIFE_STAT_STRING + " : %-15s  ", life) + String.format(Character.STAM_STAT_STRING + " :  %-15s  ", stamina)  + String.format(Locale.US, ArmorItem.ARMOR_STAT_STRING + " :  %-15s  ", this.computeProtection()) + String.format(Locale.US, BuffItem.BUFF_STAT_STRING + " :  %-15s  ", this.computeBuff()) + "(DEAD)");
		}
		
		
	}
	
	public void printStats() {
		System.out.println(this.toString());
		
		}
	
	/**
	 * Methode isAlive() qui permet de savoir si un personnage est en vie ou non
	 * @return vrai ou faux
	 */
	public boolean isAlive() {
		boolean alive = true;
		if (life <= 0) {
			alive = false;
		}
		else {
			alive = true;
		}
		return (alive);
	}
	
	/**
	 * Methode attackWith(Weapon weapon) qui permet de calculer les degats qui recevra l'adversaire selon le resultat du jet de des, 
	 * la durabilite et l'etat de l'arme ainsi que la stamina du personnage 
	 * @param weapon arme
	 * @return degats provoqu�s par l'arme
	 */
	private int attackWith(Weapon weapon) {
		int damages = 0;
		int diceRoll = this.dice.roll();
		if (weapon.isBroken()) {
			damages = 0;
		}
		if (weapon.isBroken() == false)  {
			if (diceRoll == 0) {
				damages = weapon.getMinDamage();
				}
			else if (diceRoll == 100) {
				damages = weapon.getMaxDamage();
				}
			else {
				damages = weapon.getMinDamage() + Math.round((weapon.getMaxDamage() - weapon.getMinDamage()) * ((float)diceRoll/100));
				}
			}
		if (damages > weapon.getMaxDamage()) {
			damages = weapon.getMaxDamage();
		}
		
		if (weapon.getStamCost() > this.stamina) {
			this.stamina = 0;
		}
		else {
			this.stamina = this.stamina - weapon.getStamCost();
		}
		if (this.stamina == 0 ) {
			damages = 0;
		}
		if(this.life == 0) {
			damages = 0;
		}
		weapon.use();
		System.out.print(String.format("%-15s", this.getName()) + String.format(" attacks with %-10s", weapon.toString() ) + String.format("ATTACK: %-5s ", damages) );
		return Math.round(damages * (1 + this.computeBuff()/100));
		
	}
	
	protected abstract float computeBuff();
	protected abstract float computeProtection();
	
	/**
	 * Methode attack() 
	 * @return Retourne le resultat de l'attaque 
	 * @see #attackWith(Weapon)
	 */
	public int attack() {
		return this.attackWith(this.getWeapon());
	}
	
	/**
	 * Methode getHitWith(int value) qui permet le calcul des degats re�us par un personnage selon sa protection  
	 * @param value valeur de l'attaque re�ue
	 * @return valeur
	 */
	public int getHitWith(int value) {
		if(computeProtection() > 100) {
			return 0;
		}
		value = Math.round(value*(1-computeProtection()/100)) ;
		this.life -=  this.getLife() < value ? this.getLife() : value ;
		System.out.print("DMG: ");
		return value;
	}
	
	/**
	 * Methode drink() qui permet � un personnage de restaurer sa stamina selon les stats du consommable
	 * @param drink boisson
	 * @see #use()
	 */
	private void drink(Drink drink) {
		System.out.println(this.getName() + " drinks " + drink.toString());
		this.stamina = this.getStamina() + drink.use();
		if(stamina > getMaxStamina()) {
			this.setStamina(getMaxStamina());
		}
	}
	
	/**
	 * Methode eat() qui permet � un personnage de restaurer sa vie selon les stats du consommable
	 * @param food nourriture
	 * @see #use(Consumable)
	 */
	private void eat(Food food) {
		System.out.println(this.getName() + " eats " + food.toString());
		this.life = this.getLife() + food.use();
		if(life > getMaxLife()) {
			this.setLife(getMaxLife());
		}
	}
	
	/**
	 * Methode use() qui permet d'utiliser des consommables selon leur type (nourriture ou boisson)
	 * @param consumable consommables
	 */
	public void use(Consumable consumable) {
		if (consumable instanceof Drink) {
			this.drink((Drink)consumable);
		}
		else if (consumable instanceof Food) {
			this.eat((Food)consumable);
		}
		else if (consumable instanceof RepairKit) {
			this.repairWeaponWith((RepairKit)consumable);
		}
		
	}
	
	/**
	 * Methode repairWeaponWith(RepairKit kit) qui permet de reparer une arme grace a un kit 
	 * @param kit kit de reparation
	 */
	protected void repairWeaponWith(RepairKit kit) {	
		System.out.println(this.name + " repairs " + this.weapon.toString() + " with " + kit.toString());
		if (this.weapon.getDurability() < 100) {
			this.weapon.repairWith(kit);
		}
	}
	
	
	public void consume() {
		this.use(this.getConsumable());
		
	}
	
	/*public static void main(String[] args) {
		Hero hero = new Hero("Kate");
		//Hamburger hamburger = new Hamburger();
		//hero.eat(hamburger);
		RepairKit repair = new RepairKit();
		hero.repairWeaponWith(repair);
	}*/
		

}
