package lsg.buffs.rings;

import lsg.characters.Hero;
import lsg.weapons.Sword;

/**
 * Classe RingOfSwords herite de Ring
 * @author Lysandra
 *
 */

public class RingOfSwords extends Ring{
	
	//Constructors
	/**
     * Constructeur de la classe RingOfSwords:
     * Cr�e une bague avec un nom et une puissance de 10
     */
	public RingOfSwords() {
		super("Ring of Swords", 10) ;
	}
	
	/**
     * M�thode computeBuffValue():
     * @return retourne la valeur de la puissance si le h�ros existe et poss�de une arme de type Sword
     * @see #getWeapon()
     */
	@Override
	public float computeBuffValue() {
		if (hero != null && (hero.getWeapon() instanceof Sword) )  return power ;
		else return 0f ;
		
	}
	
	/**
	 * Un test...
	 * @param args non utilis�
	 */
	public static void main(String[] args) {
		Hero hero = new Hero() ;
		RingOfSwords r = new RingOfSwords() ;
		hero.setRing(r, 1);
		hero.setWeapon(new Sword());
		System.out.println(r);
	}
}
