package lsg.buffs.rings;

import lsg.characters.Hero;

/**
 * Classe RingOfDeath herite de Ring
 * @author Lysandra
 *
 */

public class RingOfDeath extends Ring{
	
	/**
	 * limite du buff
	 */
	private static float LIMIT = 0.5f ; 

	/**
     * Constructeur de la classe RingOfDeath:
     * Cr�e une bague avec un nom et une puissance de 10000
     */
	public RingOfDeath() {
		super("Ring of Death", 10000) ;
	}

	/**
     * M�thode computeBuffValue():
     * @return si le h�ros existe, retourne la valeur de la puissance de RingOfDeath si apr�s division de la vie actuelle
     *  du h�ros et de sa vie max, la valeur gauge obtenue est inf�rieure ou �gale � la LIMIT de 0.5f 
     *  @see #getLife()
     *  @see #getMaxLife()
     *  
     */
	@Override
	public float computeBuffValue() {
		if (hero != null){
			float gauge = (float)hero.getLife() / hero.getMaxLife() ;
			if(gauge <= LIMIT) return power ;
			else return 0f ;
		}else return 0f ;
	}
	
	/**
	 * Un test...
	 * @param args non utilis�
	 */
	public static void main(String[] args) {
		Hero hero = new Hero() ;
		Ring ring = new RingOfDeath() ;
		hero.setRing(ring, 1);
		hero.getHitWith(60) ; // pour abaisser les PV du hero
		System.out.println(ring);
	}
	
}
