package lsg.buffs.talismans;

/**
 * Classe NoonGift herite de Talisman
 * @author Lysandra
 *
 */

public class NoonGift extends Talisman {
	
	//Constructors
	/**
     * Constructeur de la classe NoonGift:
     * Cr�e un talisman avec un nom, une puissance de 10.5f, et qui est actif de 12h � 14h
     */
	public NoonGift() {
		super("Noon Gift", 10.5f, 12, 14);
	}

}
