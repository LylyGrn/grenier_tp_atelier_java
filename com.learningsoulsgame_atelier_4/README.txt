ATELIER JAVA 4: 

R�ponse aux questions:

4.2: L'erreur au niveau de la classe Character est pr�sente parce que l'on a cr�e dans cette classe une 
m�thode abstraite (computeProtection()). Il faut donc mettre la classe Character en classe abstraite. 
Les erreurs au niveau des classes Hero et Monster apparaissent parce que ces deux classes h�ritent de Character 
(� l'aide du mot cl� extends), et comme une m�thode abstraite a �t� d�clar�e dans la classe Character, 
elle doit �tre d�finie au niveau de Hero et Monster.