package lsg.bags;

import java.util.HashSet;
import java.util.Iterator;

import lsg.LearningSoulsGame;
import lsg.armor.BlackWitchVeil;
import lsg.armor.DragonSlayerLeggings;
import lsg.armor.RingedKnightArmor;
import lsg.consumables.Consumable;
import lsg.consumables.food.Hamburger;
import lsg.weapons.ShotGun;
import lsg.weapons.Sword;

public class Bag {
	
	//Variables 
	
	private int capacity;
	private int weight;
	private HashSet<Collectible> items;
		
	//Getters and setters
	
	public int getCapacity() {
		return capacity;
	}

	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}

	//Constructors
	
	public Bag(int capacity) {
		this.capacity = capacity;
		this.items = new HashSet<Collectible>();
	}
	
	//Methods
	
	/**
	 * Methode <strong>push(Collectible item)</strong>:<br>
	 * Permet d'ajouter un item dans le sac
	 * @param item item � ajouter
	 */
	public void push(Collectible item) {
		if(this.weight + item.getWeight() <= this.getCapacity()) {
			items.add(item);
			setWeight(this.getWeight() + item.getWeight());
		}
	}
	
	/**
	 * Methode <strong>pop(Collectible item)</strong>:<br>
	 * Permet de retirer un item du sac
	 * @param item item � retirer
	 * @return null ou l'item
	 */
	public Collectible pop(Collectible item) {
		Iterator<Collectible> i = items.iterator();
        while(i.hasNext()) {
            Collectible collectible = i.next();
            if (collectible == item) {
                i.remove();
                this.setWeight(this.getWeight() - collectible.getWeight());
                return collectible;
            }
        }
		return null;		
	}
	
	/**
	 * Methode <strong>contains(Collectible item)</strong>: <br>
	 * indique si l�item pass� en param�tre se trouve bien dans le sac
	 */
	public boolean contains(Collectible item) {
		if (items.contains(item)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Methode <strong>getItems()</strong>
	 * @return retourne un tableau contenant les items du sac
	 */
	public Collectible[] getItems() {
		return items.toArray(new Collectible[items.size()]);		
	}
	
	/**
	 * Methode <strong>toString()</strong>
	 * @return Retourne l'affichage des statistiques
	 */
	public String toString() {
		String stats = " ";
		if(this.getWeight() != 0) {
			Iterator<Collectible> i = this.items.iterator();
			stats = this.getClass().getSimpleName() + "    [ " + this.items.size() + " items | " + this.getWeight() + "/" + this.getCapacity() + "kg ]";
			while (i.hasNext()) {
				Collectible collectible = i.next();
				stats += "\n" +  LearningSoulsGame.BULLET_POINT + collectible.toString() + "[" + collectible.getWeight() + " kg]";
			}
		}
		else {
			stats = this.getClass().getSimpleName() + "    [ " + this.items.size() + " items | " + this.getWeight() + "/" + this.getCapacity() + "kg ]" + "\n" + LearningSoulsGame.BULLET_POINT + " (empty)";
		}
		return stats;
	}
	
	/**
	 * Methode <strong>transfer(Bag from, Bag into)</strong>
	 * @param from sac source
	 * @param into sac de destination
	 */
	public static void transfer(Bag from, Bag into) {
		for(Collectible collectible: from.getItems()){
            into.push(collectible);
            if(into.contains(collectible)){
                from.pop(collectible);
            }
        }
	}
	
	

	public static void main(String[] args) {
		/*SmallBag smb = new SmallBag(10);
		BlackWitchVeil veil = new BlackWitchVeil();
		Hamburger ham = new Hamburger();
		Sword sw = new Sword();
		DragonSlayerLeggings ds = new DragonSlayerLeggings();
		smb.push(veil);
		smb.push(ham);
		smb.push(sw);
		smb.push(ds);
		System.out.println(smb.toString());
		System.out.println(" \nPop sur " + ds.toString());
		smb.pop(ds);
		System.out.println();
		System.out.println(smb.toString());*/
		
		System.out.println("Sac 1 : ");
		Bag bag = new Bag(10);
		ShotGun shotgun = new ShotGun();
		DragonSlayerLeggings ds = new DragonSlayerLeggings();
		RingedKnightArmor armor = new RingedKnightArmor();
		bag.push(armor);
		bag.push(shotgun);
		bag.push(ds);
		System.out.println(bag.toString());
		
		System.out.println("\nSac 2 : ");
		Bag bag2 = new Bag(5);
		System.out.println(bag2.toString());
		
		System.out.println("\nSac 2 apr�s transfert : ");
		transfer(bag, bag2);
		System.out.println(bag2.toString());
		
		System.out.println("\nSac 1 apr�s transfert : ");
		System.out.println(bag.toString());
		
	}

}
