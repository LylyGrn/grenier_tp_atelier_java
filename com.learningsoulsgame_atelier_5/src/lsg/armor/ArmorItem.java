package lsg.armor;

/**
 * Classe ArmorItem concerne les armures 
 * @author Lysandra
 *
 */

public class ArmorItem {
	//Variables
	
	/**
	 * nom de l'armure
	 */
	protected String name;
	
	/**
	 * valeur de l'armure
	 */
	protected float armorValue;
	
	public final static String ARMOR_STAT_STRING = "PROTECTION";
	
	
	//Getters and Setters
	
	/**
	 * Methode getName()
	 * @return retourne le nom de l'armure
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Methode getArmorValue()
	 * @return retourne la valeur de l'armure
	 */
	public float getArmorValue() {
		return armorValue;
	}
	
	//Constructors
	
	/**
	 * Constructeur qui permet de donner un nom et de déterminer la valeur d'une armure au moment de son instanciation
	 * @param name nom de l'armure
	 * @param armorValue valeur de l'armure
	 */
	public ArmorItem(String name, float armorValue) {
		this.name = name;
		this.armorValue = armorValue;
		
	}
	
	//Methods 
	
	/**
	 * Methode toString() pour les armures
	 * @return Retourne les statistiques d'une armure sous la forme "nom (valeur de l'armure)"
	 */
	public String toString() {
		return (this.getName() + "(" + this.getArmorValue() + ")");
		
	}
	
	/**
	 * Methode printStats() qui affiche les statistiques d'une armure
	 * @see #toString()
	 */
	public void printStats() {
		System.out.println(this.toString());
		
		}
	

}
