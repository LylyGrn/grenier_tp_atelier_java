package lsg.consumables;

import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Whisky;
import lsg.consumables.drinks.Wine;
import lsg.consumables.food.Americain;
import lsg.consumables.food.Hamburger;

public class MenuBestOfV1 {
	
	//Variables
	
	protected Consumable[] menu = new Consumable[5];
	
	//Constructors
	
	public MenuBestOfV1() {
		Hamburger hamburger = new Hamburger();
		Wine wine = new Wine();
		Americain americain = new Americain();
		Coffee coffee = new Coffee();
		Whisky whisky = new Whisky();
		menu[0] = hamburger;
		menu[1] = wine;
		menu[2] = americain;
		menu[3] = coffee;
		menu[4] = whisky;
		
	}
	
	//Methods 
	
	public String toString() {
		String menuBest = "MenuBestOfV1 : \n";
		for (int index = 0; index < this.menu.length ; index++) {
			menuBest += index+1 + " : " + menu[index] + "\n";
		}
		return menuBest;
	}
	
	
	public static void main(String[] args) {
		MenuBestOfV1 menuA = new MenuBestOfV1();
		System.out.println(menuA.toString());
	}

}
