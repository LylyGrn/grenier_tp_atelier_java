package characters;

public class Hero {
	
	private String name;
	private int life = 100;
	private int maxLife = 100;
	private int stamina = 50;
	private int maxStamina = 50;
	
	//Getters and Setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getLife() {
		return life;
	}
	public void setLife(int life) {
		this.life = life;
	}
	
	public int getMaxLife() {
		return maxLife;
	}
	public void setMaxLife(int maxLife) {
		this.maxLife = maxLife;
	}
	
	public int getStamina() {
		return stamina;
	}
	public void setStamina(int stamina) {
		this.stamina = stamina;
	}
	
	public int getMaxStamina() {
		return maxStamina;
	}
	public void setMaxStamina(int maxStamina) {
		this.maxStamina = maxStamina;
	}
	
	
	//Constructors
	
	public Hero(String name) {
		this.name = name;
	}
	public Hero() {
		name = "Gregooninator";
	}
	
	
	//Methods
	
	
	public String toString() {
		if (isAlive() == true) {
			return("[ Hero ]\t" + name + "\tLIFE: " + maxLife + "  STAMINA: " + maxStamina + "\t(ALIVE)");
		}
		else {
			return("[ Hero ]\t" + name + "\tLIFE: " + maxLife + "  STAMINA: " + maxStamina + "\t(DEAD)");
		}
		
		
	}
	public void printStats() {
		System.out.println(this.toString());
		
		}
	
	public boolean isAlive() {
		boolean alive = true;
		if (life <= 0) {
			alive = false;
		}
		else {
			alive = true;
		}
		return (alive);
	}
	


}
