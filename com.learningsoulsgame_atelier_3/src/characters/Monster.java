package characters;

public class Monster extends Character {
	
	//Variables
	protected static int INSTANCES_COUNT = 1;
	
	
	
	
	//Constructors
	
	public Monster(String name) {
		super(name);
		Monster.INSTANCES_COUNT++;
		this.setLife(10);
		this.setMaxLife(10);
		this.setStamina(10);
		this.setMaxStamina(10);
		
	}
	public Monster() {
		this("Monster_" + INSTANCES_COUNT);
		

	}
		

}
