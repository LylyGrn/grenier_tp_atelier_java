package lsg.buffs.talismans;

/**
 * Classe BuffItem herite de Talisman
 * @author Lysandra
 *
 */

public class MoonStone extends Talisman {
	
	//Constructors
	/**
     * Constructeur de la classe MoonStone:
     * Cr�e un talisman avec un nom, une puissance de 20f, et qui est actif de 23h � 3h
     */
	public MoonStone() {
		super("Moon Stone", 20f, 21, 3);
	}
	
}
