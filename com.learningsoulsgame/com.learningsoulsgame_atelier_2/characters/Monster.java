package characters;

public class Monster {
	
	//Variables
	private String name;
	private int life = 10;
	private int maxLife = 10;
	private int stamina = 10;
	private int maxStamina = 10;
	static int INSTANCES_COUNT = 1;
	
	
	//Getters and Setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getLife() {
		return life;
	}
	public void setLife(int life) {
		this.life = life;
	}
	
	public int getMaxLife() {
		return maxLife;
	}
	public void setMaxLife(int maxLife) {
		this.maxLife = maxLife;
	}
	
	public int getStamina() {
		return stamina;
	}
	public void setStamina(int stamina) {
		this.stamina = stamina;
	}
	
	public int getMaxStamina() {
		return maxStamina;
	}
	public void setMaxStamina(int maxStamina) {
		this.maxStamina = maxStamina;
	}
	
	
	//Constructors
	
	public Monster(String name) {
		this.name = name;
		Monster.INSTANCES_COUNT++;
	}
	public Monster() {
		this.name = "Monster_" + INSTANCES_COUNT++;

	}
	
	
	//Methods
	public String toString() {
		if (isAlive() == true) {
			return("[ Monster ]\t" + name + "\tLIFE: " + maxLife + "  STAMINA: " + maxStamina + "\t(ALIVE)");
		}
		else {
			return("[ Monster ]\t" + name + "\tLIFE: " + maxLife + "  STAMINA: " + maxStamina + "\t(DEAD)");
		}
		
		
	}
	public void printStats() {
		System.out.println(this.toString());
		
		}
	
	public boolean isAlive() {
		boolean alive = true;
		if (life <= 0) {
			alive = false;
		}
		else {
			alive = true;
		}
		return alive;
	}

}
