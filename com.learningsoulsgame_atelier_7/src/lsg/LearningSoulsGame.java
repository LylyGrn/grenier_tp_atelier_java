package lsg;

import lsg.characters.Hero;
import lsg.characters.Lycanthrope;
import lsg.characters.Monster;
import lsg.consumables.Consumable;
import lsg.consumables.MenuBestOfV4;
import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Whisky;
import lsg.consumables.food.Hamburger;
import lsg.consumables.repair.RepairKit;
import lsg.weapons.Sword;
import lsg.characters.Character;
import lsg.weapons.Weapon;
import lsg.weapons.ShotGun;
import lsg.weapons.*;
import lsg.armor.*;
import lsg.bags.MediumBag;
import lsg.bags.SmallBag;
import lsg.buffs.*;
import lsg.buffs.rings.DragonSlayerRing;
import lsg.buffs.rings.Ring;
import lsg.buffs.rings.RingOfDeath;
import lsg.buffs.rings.RingOfSwords;
import lsg.buffs.talismans.MoonStone;
import lsg.buffs.talismans.Talisman;
import lsg.consumables.*;

import java.util.Iterator;
import java.util.Scanner;

import javax.security.auth.Refreshable;

/**
 * Classe <strong>LearningSoulsGame</strong> permettant de tester l'ensemble des methodes pour lancer un combat et afficher les stats
 * @author Lysandra 
 *
 */

public class LearningSoulsGame {
	
	//Variables
	public Hero hero;
	public Monster monster;
	public Scanner scanner;
	public static final String BULLET_POINT = "\u2219";

	//Methods
	/**
	 * Methode <strong>refresh()</strong>:<br>
	 * Cette methode affiche les statistiques des personnages
	 */
	public void refresh() {
		this.hero.printStats();
		System.out.println(this.hero.armorToString());
		this.hero.printRings();
		this.hero.printConsumable();
		System.out.println(this.hero.getWeapon().toString());
		this.hero.printBag();
		System.out.println();
		this.monster.printStats();
		System.out.println(this.monster.getWeapon().toString());
	}
	
	/**
	 * Methode <strong>fight1v1()</strong>:<br>
	 * Cette methode lance un combat entre deux personnages jusqu'� ce que l'un des deux meurt
	 */
	public void fight1v1() {
		Character firstCharacter = this.hero;
		Character opponent = this.monster;
		int action;
		refresh();
		Scanner scanner = new Scanner(System.in);
		while (firstCharacter.isAlive() && opponent.isAlive()){
			System.out.println();
			System.out.print("Hero's action for next move :  (1) attack | (2) consume > ");
			action = scanner.nextInt();
			while(action != 1 && action != 2) {
				System.out.print("Hero's action for next move :  (1) attack | (2) consume > ");
				action = scanner.nextInt();
			}
				if (action == 1) {
					int heroAttack = opponent.getHitWith(firstCharacter.attack());
					System.out.println(heroAttack);
				}
				else if (action == 2) {
					
					firstCharacter.consume();										
				}
			scanner.nextLine();			
			
			System.out.println();
			refresh();
			System.out.println();	
			int opponentAttack = firstCharacter.getHitWith(opponent.attack());
			System.out.println(opponentAttack);
			System.out.println();
			refresh();
		}
		if(firstCharacter.isAlive()) {
			System.out.println("-- " + firstCharacter.getName() + "     WINS !!!--");
		}
		else {
			System.out.println("-- " + opponent.getName() + "     WINS !!!--");
		}
		scanner.close(); 
		
	}
	
	/**
	 * Methode <strong>init()</strong>:<br>
	 * Cette methode permet d'initialiser les personnages, armures, armes et buffs
	 */
	public void init() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem armor = new RingedKnightArmor();
		this.hero.setArmorItem(veil, 1);
		this.hero.setArmorItem(armor, 3);
		Hamburger hamburger = new Hamburger();
		this.hero.setConsumable(hamburger);
		this.monster = new Lycanthrope();
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		title();
		System.out.println();
		
	}
	
	/**
	 * Methode <strong>play_v1()</strong>:<br>
	 * Cette methode initialise une partie et lance un combat
	 * @see #init()
	 * @see #fight1v1()
	 */
	public void play_v1() {
		init();
		fight1v1();
		
	}
	/**
	 * Methode <strong>play_v2()</strong>:<br>
	 * Cette methode initialise manuellement les personnages, armes, armures et lance un combat
	 * @see #fight1v1()
	 */
	public void play_v2() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem armor = new RingedKnightArmor();
		this.hero.setArmorItem(veil, 1);
		this.hero.setArmorItem(armor, 3);
		this.monster = new Monster("Jack the Ripper");
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		fight1v1();
		
	}
	
	/**
	 * Methode <strong>play_v3()</strong>:<br>
	 * Cette methode initialise manuellement les personnages, armes, armures, bufss et lance un combat
	 * @see #fight1v1()
	 */
	public void play_v3() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		ArmorItem veil = new BlackWitchVeil();
		ArmorItem armor = new DragonSlayerLeggings();
		this.hero.setArmorItem(veil, 1);
		this.hero.setArmorItem(armor, 3);
		Ring ring = new DragonSlayerRing();
		hero.setRing(ring, 1);
		this.monster = new Lycanthrope();
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		this.monster.setTalisman(new MoonStone(), 1);
		fight1v1();	
	}
	
	/**
	 * Methode <strong>createExhaustedHero()</strong>:<br>
	 * Methode qui cree un heros epuise
	 * 
	 */
	public void createExhaustedHero() {
		this.hero = new Hero("NewOne");
		Weapon weapon = new Weapon("Grosse Arme", 0, 0, 1000, 100);
		this.hero.setWeapon(weapon);
		this.hero.getHitWith(99);
		System.out.println();
		this.hero.attack();
		System.out.println();
		this.hero.printStats();
	}

	
	/**
	 * Methode <strong>title()</strong>:<br>
	 * Methode qui permet d'afficher le titre du jeu
	 */
	public void title() {
		System.out.println("###############################");
		System.out.println("#   THE LEARNING SOULS GAME   #");
		System.out.println("###############################");
	}
	
	public void testExceptions() {
		this.hero.setWeapon(null);
		fight1v1();
	}
	
	public static void main(String[] args) {
		LearningSoulsGame firstGame = new LearningSoulsGame();
		firstGame.init();
		firstGame.testExceptions();
	}

}
