package lsg.bags;

public interface Collectible {
	
	
	//Methods
	
	/**
	 * Methode <strong>getWeight()</strong> 
	 * @return un entier correspondant au nombre de kilos du collectable
	 */
	public int getWeight();

}
