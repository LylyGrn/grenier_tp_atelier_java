package lsg.weapons;
import lsg.weapons.Weapon;

/**
 * Classe Claw herite de Weapon
 * @author Lysandra
 *
 */

public class Claw extends Weapon{
	
	//Constructor
	/**
	 * Constructeur de la classe Claw qui initialise un nom, des d�gats min, max, un cout de stamina et une durabilite
	 */
	public Claw() {
		super("Bloody Claw", 50, 150, 5, 100);
	}

}
