package lsg.weapons;

public class Weapon {
	
	//Variables
	private String name;
	private int minDamage;
	private int maxDamage;
	private int stamCost;
	private int durability;
	
	//Constructor
	public Weapon(String name, int minDamage, int maxDamage, int stamCost, int durability) {
		this.name= name;
		this.minDamage = minDamage;
		this.maxDamage = maxDamage;
		this.stamCost = stamCost;
		this.durability = durability;		
	}
	
	public Weapon() {
		
	}
	
	//Getters and Setters
	public String getName() {
		return name;
	}
	
	public int getMinDamage() {
		return minDamage;
	}
	
	public int getMaxDamage() {
		return maxDamage;
	}
	
	public int getStamCost() {
		return stamCost;
	}
	
	public int getDurability() {
		return durability;
	}
	private void setDurability(int durability) {
		this.durability = durability;
	}
	
	//Methods
	public int use() {
		return this.durability -= 1;
	}
	
	public boolean isBroken() {
		return this.durability <=0;
	}
	
	public String toString() {
		return (String.format("%-2s", name) + " (min:" + minDamage + " max:" + maxDamage + " stam:" + stamCost + " dur:" + durability + ") ");
		
	}

}
