package lsg.characters;
import lsg.helpers.Dice;
import lsg.weapons.Weapon;
import java.lang.Math;
import java.util.Locale; 

public abstract class Character {
	
	//Variables
	protected String name;
	protected int life;
	protected int maxLife;
	protected int stamina;
	protected int maxStamina;
	protected Dice dice;
	protected Weapon weapon;
	
	//Getters and Setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getLife() {
		return life;
	}
	public void setLife(int life) {
		this.life = life;
	}
	
	public int getMaxLife() {
		return maxLife;
	}
	public void setMaxLife(int maxLife) {
		this.maxLife = maxLife;
	}
	
	public int getStamina() {
		return stamina;
	}
	public void setStamina(int stamina) {
		this.stamina = stamina;
	}
	
	public int getMaxStamina() {
		return maxStamina;
	}
	public void setMaxStamina(int maxStamina) {
		this.maxStamina = maxStamina;
	}
	
	public Weapon getWeapon() {
		return weapon;
	}
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}
	
	//Constructor
	public Character(String name) {
		this.name = name;
		this.dice = new Dice(101);
		this.weapon = new Weapon("Basic Sword", 5, 10, 20, 100);
	}
	
	public Character() {
		
	}
	
	//Methods

	public String toString() {
		if (isAlive() == true) {
			return (String.format("[ %-7s ] ", getClass().getSimpleName()) + String.format("   %-20s", name) + String.format("LIFE:  %-15s  ", life) + String.format("STAMINA:  %-15s  ", stamina)  + String.format(Locale.US, "PROTECTION:  %-15s  ", this.computeProtection()) + String.format(Locale.US, "BUFF:  %-15s  ", this.computeBuff()) +"(ALIVE)");
		}
		else {
			return (String.format("[ %-7s ] ", getClass().getSimpleName()) + String.format("   %-20s", name) + String.format("LIFE:  %-15s  ", life) + String.format("STAMINA:  %-15s  ", stamina)  + String.format(Locale.US, "PROTECTION:  %-15s  ", this.computeProtection()) + String.format(Locale.US, "BUFF:  %-15s  ", this.computeBuff()) + "(DEAD)");
		}
		
		
	}
	
	public void printStats() {
		System.out.println(this.toString());
		
		}
	
	public boolean isAlive() {
		boolean alive = true;
		if (life <= 0) {
			alive = false;
		}
		else {
			alive = true;
		}
		return (alive);
	}
	
	private int attackWith(Weapon weapon) {
		int damages = 0;
		int diceRoll = this.dice.roll();
		if (weapon.isBroken()) {
			damages = 0;
		}
		if (weapon.isBroken() == false)  {
			if (diceRoll == 0) {
				damages = weapon.getMinDamage();
				}
			else if (diceRoll == 100) {
				damages = weapon.getMaxDamage();
				}
			else {
				damages = weapon.getMinDamage() + Math.round((weapon.getMaxDamage() - weapon.getMinDamage()) * ((float)diceRoll/100));
				}
			}
		if (damages > weapon.getMaxDamage()) {
			damages = weapon.getMaxDamage();
		}
		
		if (weapon.getStamCost() > this.stamina) {
			this.stamina = 0;
		}
		else {
			this.stamina = this.stamina - weapon.getStamCost();
		}
		if (this.stamina == 0 ) {
			damages = 0;
		}
		if(this.life == 0) {
			damages = 0;
		}
		weapon.use();
		System.out.print(String.format("%-15s", this.getName()) + String.format(" attacks with %-10s", weapon.toString() ) + String.format("ATTACK: %-5s ", damages) );
		return Math.round(damages * (1 + this.computeBuff()/100));
		
	}
	
	protected abstract float computeBuff();
	protected abstract float computeProtection();
	
	public int attack() {
		return this.attackWith(this.getWeapon());
	}
	
	public int getHitWith(int value) {
		if(computeProtection() > 100) {
			return 0;
		}
		value = Math.round(value*(1-computeProtection()/100)) ;
		this.life -=  this.getLife() < value ? this.getLife() : value ;
		System.out.print("DMG: ");
		return value;
	}
	
	
		

}
