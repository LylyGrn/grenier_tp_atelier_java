package atelier_1;

//Exercice Programme Scope

public class Scope {
	
	public static void main(String[] args){ //correction: : les crochets se mettent plut�t apr�s le tableau String
		int nombre = 0; //la variable i est d�j� utilis�e, il faut donc changer le nom, ici nombre.
		for (int i = 0; i<5; i++){
			nombre++; //cette variable doit �tre incr�ment�e 
			System.out.print(nombre + ", ");
		}
		System.out.print("\n");
	}
		
}