package lsg.consumables.repair;

import lsg.consumables.Consumable;
import lsg.weapons.Weapon;

public class RepairKit extends Consumable {

	//Constructors
	public RepairKit() {
		super("Repair Kit", 10, Weapon.DURABILITY_STAT_STRING);
	}
	
	/**
	 * Methode <strong>use()</strong>:<br>
	 * Surcharge de la methode <strong>use()</strong> qui ajoute un point � chaque utilisation  
	 */
	public int use() {
		int capacity = this.getCapacity();
		this.setCapacity(capacity - 1);
		return 1;
		
	}
	

}
