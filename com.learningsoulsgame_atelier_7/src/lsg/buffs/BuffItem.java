package lsg.buffs;

import java.util.Locale;

import lsg.bags.Collectible;

/**
 * Classe abstraite BuffItem concerne les buffs
 * @author Lysandra
 *
 */

public abstract class BuffItem implements Collectible{
	
	/**
	 * nom du buff
	 */
	private String name ; 
	
	public final static String BUFF_STAT_STRING = "BUFF";
	
	//Constructors
	/**
     * Constructeur de la classe BuffItem
     * @param name nom du buff
     */
	public BuffItem(String name) {
		this.name = name ;
	}

	//Methods
	
	public abstract float computeBuffValue() ;
	
	/**
	 * Methode <strong>getName()</strong>
	 * @return retourne le nom du buff
	 */
	public String getName() {
		return name;
	}
	
	@Override
	/**
	 * Methode <strong>toString()</strong> pour les buffs
	 * @returns Retourne les statistiques d'un buff sous la forme "[nom, valeur du buff]"
	 */
	public String toString() {
		return String.format(Locale.US, "[%s, %.2f]", getName(), computeBuffValue()) ;
	}
	
	/**
	 * Methode <strong>printStats()</strong>:
	 * Methode qui affiche les statistiques d'un buff
	 * @see #toString()
	 */
	public void printStats() {
		System.out.println(this.toString());
		
		}
	
	/**
	 * Methode <strong>getWeight()</strong>
	 * @return un poids de 1 kg
	 */
	@Override
	public int getWeight() {
		return 1;
	}
	
	
}
