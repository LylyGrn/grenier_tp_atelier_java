package lsg.armor;

import lsg.bags.Collectible;

/**
 * Classe ArmorItem concerne les armures 
 * @author Lysandra
 *
 */

public class ArmorItem implements Collectible {
	//Variables
	
	/**
	 * nom de l'armure
	 */
	protected String name;
	
	/**
	 * valeur de l'armure
	 */
	protected float armorValue;
	
	public final static String ARMOR_STAT_STRING = "PROTECTION";
	
	
	//Getters and Setters
	
	/**
	 * Methode <strong>getName()</strong>
	 * @return retourne le nom de l'armure
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Methode <strong>getArmorValue()</strong>
	 * @return retourne la valeur de l'armure
	 */
	public float getArmorValue() {
		return armorValue;
	}
	
	//Constructors
	
	/**
	 * Constructeur qui permet de donner un nom et de déterminer la valeur d'une armure au moment de son instanciation
	 * @param name nom de l'armure
	 * @param armorValue valeur de l'armure
	 */
	public ArmorItem(String name, float armorValue) {
		this.name = name;
		this.armorValue = armorValue;
		
	}
	
	//Methods 
	
	/**
	 * Methode <strong>toString()</strong> pour les armures
	 * @return Retourne les statistiques d'une armure sous la forme "nom (valeur de l'armure)"
	 */
	public String toString() {
		return (this.getName() + "(" + this.getArmorValue() + ")");
		
	}
	
	/**
	 * Methode <strong>printStats()</strong> qui affiche les statistiques d'une armure
	 * @see #toString()
	 */
	public void printStats() {
		System.out.println(this.toString());
		
		}

	/**
	 * Methode <strong>getWeight()</strong>
	 * @return un poids de 4 kg
	 */
	@Override
	public int getWeight() {
		return 4;
	}
	

}
