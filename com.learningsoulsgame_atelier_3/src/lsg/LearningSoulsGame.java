package lsg;

import characters.Hero;
import characters.Monster;
import lsg.weapons.Sword;
import characters.Character;
import lsg.weapons.Weapon;
import lsg.weapons.ShotGun;
import lsg.weapons.*;
import java.util.Scanner;

public class LearningSoulsGame {
	
	//Variables
	public Hero hero;
	public Monster monster;
	public Scanner scanner;

	//Methods
	public void refresh() {
		this.hero.printStats();
		this.monster.printStats();
	}
	
	public void fight1v1() {
		Character firstCharacter = this.hero;
		Character opponent = this.monster;
		refresh();
		Scanner scanner = new Scanner(System.in);
		while (firstCharacter.isAlive() && opponent.isAlive()){
			System.out.println();
			System.out.println("Hit enter key for next move >  ");
			scanner.nextLine();			
			int heroAttack = opponent.getHitWith(firstCharacter.attack());
			System.out.println(heroAttack);
			System.out.println();
			refresh();
			System.out.println();
			System.out.println("Hit enter key for next move >  ");
			scanner.nextLine();	
			int opponentAttack = firstCharacter.getHitWith(opponent.attack());
			System.out.println(opponentAttack);
			System.out.println();
			refresh();
		}
		if(firstCharacter.isAlive()) {
			System.out.println("-- " + firstCharacter.getName() + "     WINS !!!--");
		}
		else {
			System.out.println("-- " + opponent.getName() + "     WINS !!!--");
		}
		scanner.close(); 
		
	}
	
	public void init() {
		this.hero = new Hero("Kate Kane");
		Weapon sword = new Sword();
		this.hero.setWeapon(sword);
		this.monster = new Monster("Jack the Ripper");
		Weapon claw = new Claw();
		this.monster.setWeapon(claw);
		
	}
	
	public void play_v1() {
		init();
		fight1v1();
		
	}
	
	
	public static void main(String[] args) {
		//Character firstHero = new Hero();
		//Hero secondHero = new Hero("KateKane");
		//Monster monster = new Monster();
		//Monster monster1 = new Monster("JacktheRipper");
		//Monster monster2 = new Monster();
		//Weapon sword = new Sword();
		//Hero rick = new Hero("Rick");
		//Weapon shotGun = new ShotGun();
		//Weapon claw = new Claw();
		//rick.setWeapon(shotGun);
		//monster1.setWeapon(claw);
		//firstHero.printStats();
		//monster.printStats();
		//monster2.printStats();
		//secondHero.printStats();
		
		//monster1.printStats();
		//rick.getHitWith(monster1.attack());
		//rick.printStats();
		//monster1.getHitWith(rick.attack());
		//monster1.printStats();
		LearningSoulsGame firstGame = new LearningSoulsGame();
		firstGame.play_v1();

	}

}
