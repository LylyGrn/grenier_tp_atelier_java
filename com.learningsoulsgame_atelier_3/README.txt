ATELIER JAVA 3: 

R�ponse aux questions:

1.2:
- La m�thode getClasse() permet de r�cup�rer le nom complet d'une classe (jusqu'au package auquel elle appartient).
La m�thode getSimpleName() permet de r�cup�rer le nom de la classe de mani�re simplifi�, donc seulement son nom. 
Ici, cette m�thode r�cup�re donc Hero ou Monster. 

- Il faut passer des membres de private � protected pour que les classes qui h�ritent de la classe Character
puissent acc�der aux attributs, qui deviennent donc visibles de ces classes. 

6:
- Apr�s avoir instanci� un Hero, un Monster et une Sword et lanc� plusieurs attaques avec chacun, on remarque
que la durabilit� de l'�p�e diminue comme si les personnages attaquaient � tour de r�le avec la m�me arme
(qu'ils partageraient donc). En d'autres termes, apr�s une attaque du h�ros, la durabilit� est � 99, et ensuite,
apr�s l'attaque du monstre, la durabilit� est de 98 au lieu de 99. C'est le cas, car une seule arme a �t� cr��e, 
les deux personnages la partagent donc. 