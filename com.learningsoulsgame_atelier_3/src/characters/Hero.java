package characters;

public class Hero extends Character {

	//Constructors
	
	public Hero(String name) {
		super(name);
		this.setLife(100);
		this.setMaxLife(100);
		this.setStamina(50);
		this.setMaxStamina(50);
		
	}
	public Hero() {
		this.name = "Gregooninator";
		
	}
	

}
